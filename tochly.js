
var SowaTorchly = SowaTorchly || (function() {
    "use strict";
    var torch_on_img = "https://s3.amazonaws.com/files.d20.io/images/6671/thumb.png?1336345350";
    var torch_off_img = "https://s3.amazonaws.com/files.d20.io/images/45557/thumb.png?1340049624";
    function makeNormalMessage(text) {
	  return '<div style="border: 1px solid black; background-color: white; padding: 3px 3px;">' +
	    '<div style="font-weight: bold; border-bottom: 1px solid black;font-size: 130%;">' +
	    `Expend spell slots </div>${text}</div>`;
	}

	function makeErrorMessage(text) {
	  return '<div style="border: 1px solid black; background-color: white; padding: 3px 3px;">' +
	    '<div style="font-weight: bold; border-bottom: 1px solid black;font-size: 130%;color:red;">' +
	    `Expend spell slots </div>${text}</div>`;
	}

    var error = function(player_id, error) {
        let player = getObj("player", player_id);
        let player_name = player.get("displayname");
        sendChat("SYSTEM", "/w \"" + player_name + "\" " + error);
    };

    var get_tokens_torch = function(obj) {
        if(obj.get("represents") === "") {
            return
        }
        var torch_id = getAttrByName(obj.get("represents"), 'sowa_torch');
        if(torch_id === undefined) {

            return
        }

        var torch = getObj("graphic", torch_id);
        if(torch === undefined) {
            return;
        }
        return torch;
    };

    var get_selected_token = function(msg) {
        var player_id = msg.playerid;
        var selected = msg.selected;
        log(player_id)
        log(selected);
        if(selected === undefined) {
            error(player_id, makeErrorMessage("Select token"));
            return;
        }
        if(selected.length !== 1) {
            error(player_id, makeErrorMessage("Select only one token"));
            return;
        }
        selected = selected[0];
        selected = getObj(selected._type, selected._id);
        return selected;
    }

    var handle_wield_torch = function(msg) {
        var player_id = msg.playerid;
        var selected = get_selected_token(msg);
        log(selected);
        if(selected === undefined) {
            return;
        }
        var character_id = selected.get("represents");
        var torch = createObj('graphic', {
                            subtype: "token",
                            imgsrc: torch_on_img,
                            pageid: selected.get("pageid"),
                            layer: "objects",
                            left: selected.get("left")+20,
                            top: selected.get("top"),
                            width: selected.get("width"),
                            height: selected.get("height"),
                            light_otherplayers: true
        });
        toFront(torch);
        var attr_name = "sowa_torch";
        var torch_attr = findObjs({type: "attribute", characterid: character_id, name: attr_name});
        if(torch_attr.length !== 1) {
            error(player_id, makeErrorMessage("Wrong spell slot"));
            var torch_attr = createObj('attribute', {
                                        characterid: character_id,
                                        name: "sowa_torch",
                                        current: torch.get("id")
            });
        }
        else {
                torch_attr = torch_attr[0];
                torch_attr.set("current", torch.get("id"));
            }

    };

    var handle_light_torch = function(msg) {
        var selected = get_selected_token(msg);
        if(selected === undefined) {
            return;
        }
        var torch = get_tokens_torch(selected);
        if(torch === undefined) {
            return;
        }
        torch.set("light_radius", 40);
        torch.set("light_dimradius", 20);
    };

    var handle_quench_torch = function(msg) {
        var selected = get_selected_token(msg);
        if(selected === undefined) {
            return;
        }
        var torch = get_tokens_torch(selected);
        if(torch === undefined) {
            return;
        }
        torch.set("light_radius", 0);
        torch.set("light_dimradius", 0);
    };

    var handle_drop_torch = function(msg) {
        var player_id = msg.playerid;
        var selected = get_selected_token(msg);
        if(selected === undefined) {
            return;
        }
        var character_id = selected.get("represents");
        var torch = get_tokens_torch(selected);
        if(torch === undefined) {
            return;
        }
        var attr_name = "sowa_torch";
        var torch_attr = findObjs({type: "attribute", characterid: character_id, name: attr_name});
        if(torch_attr.length !== 1) {
            error(player_id, makeErrorMessage("Wrong spell slot"));
        }
        else {
                torch_attr = torch_attr[0];
                torch_attr.set("current", "");
            }
    };

    var move_torch = function(obj, prev) {
        var torch = get_tokens_torch(obj);
        if(torch === undefined) {
            return;
        }
        torch.set("top", obj.get("top"));
        torch.set("left", obj.get("left")+20)
        toFront(obj);
        toFront(torch);

    };

    var handle_input = function(msg_orig) {
        var msg = _.clone(msg_orig);
        if (msg.type !== "api") {
            return;
        }
        if(msg.content.match(/^!sowa-wield-torch$/)) {
            handle_wield_torch(msg);
        }
        else if(msg.content.match(/^!sowa-light-torch$/)) {
            handle_light_torch(msg);
        }
        else if(msg.content.match(/^!sowa-quench-torch$/)) {
            handle_quench_torch(msg);
        }
        else if(msg.content.match(/^!sowa-drop-torch$/)) {
            handle_drop_torch(msg);
        }
    };

    var register_event_handlers = function() {
        on("chat:message", handle_input);
        on("change:graphic", move_torch);
    };

    return {
        RegisterEventHandlers: register_event_handlers
    };

})();

on("ready", function() {
    "use strict";
    SowaTorchly.RegisterEventHandlers();
    log("We ready bois!");
});