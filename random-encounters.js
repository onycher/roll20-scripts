var SowaRandomEncounters = SowaRandomEncounters || function () {

        function get_page(args) {
            var pages = findObjs({type: 'page', name: args.page_name});
            return page = _.first(pages);
        }

        function clean_img_src(imgsrc) {
            var parts = imgsrc.match(/(.*\/images\/.*)(thumb|med|max)(.*)$/);
            if (parts) {
                return parts[1] + 'thumb' + parts[3];
            }

        }

        function get_imgsrc_from_table(args) {
            var roll_tables = findObjs({type: 'rollabletable', name: args.roll_table});
            if (roll_tables.length !== 1) {
                log("SowaRandomEncounters: Rollable table name incorrect");
                return
            }
            var roll_table = _.first(roll_tables);
            var images = [];
            var what = _.each(args.roll_name.split(','), (name) => {images.push(findObjs({
                    type: 'tableitem',
                    rollabletableid: roll_table.get('id'),
                    name: name
                }))
        })
            images = _.flatten(images);
            if (images.length === 0) {
                return
            }
            return _.sample(images).get('avatar')
        }

        function fill_background(args) {
            var page = get_page(args);
            if (page === undefined) {
                log('SowaRandomEncounters: Wrong page name');
                return
            }
            var image = get_imgsrc_from_table(args);
            if (image === undefined) {
                log("SowaRandomEncounters: Wrong image");
                return
            }
            image = clean_img_src(image);

            var center_width = 35 * args.bg_width;
            var center_height = 35 * args.bg_height;
            for (i = 0; i < page.get('width') / args.bg_width; i++) {
                for (j = 0; j < page.get('height') / args.bg_height; j++) {

                    createObj('graphic', {
                        subtype: 'token',
                        pageid: page.id,
                        imgsrc: image,
                        top: center_height + 70 * j * args.bg_height,
                        left: center_width + 70 * i * args.bg_width,
                        width: 70 * args.bg_width,
                        height: 70 * args.bg_height,
                        layer: 'map'
                    })
                }
            }

        }

        function spawm_trees(args) {
            var page = get_page(args);
            if (page === undefined) {
                log('SowaRandomEncounters: Wrong page name');
                return
            }
            for (i = 0; i < page.get('width') / args.sparsity; i++) {
                for (j = 0; j < page.get('height') / args.sparsity; j++) {
                    var location_1 = _.random(args.height / 2, (args.sparsity) * 70 - args.height / 2);
                    var location_2 = _.random(args.width / 2, (args.sparsity) * 70 - args.height / 2);
                    var image = get_imgsrc_from_table(args);
                    if (image === undefined) {
                        log('SOWARandomEncounters: Wrong image');
                        return;
                    }
                    image = clean_img_src(image);
                    var r_size = _.random(args.r_size)
                    createObj('graphic', {
                        subtype: 'token',
                        pageid: page.id,
                        imgsrc: image,
                        top: location_1 + j * 70 * args.sparsity,
                        left: location_2 + i * 70 * args.sparsity,
                        width: args.width + r_size,
                        height: args.height + r_size,
                        layer: 'map',
                        rotation: _.random(360)
                    })
                }
            }
        }

        function handle_input(msg) {
            if (msg.type !== 'api') {

            }
            else if (msg.content.startsWith('!sowa-fill-background')) {
                try {
                    if (msg.content.split(' ').length !== 6) {
                        log('SowaRandomEncounters: !sowa-fill-background page_name bg_width bg_height roll_table roll_name');
                        return
                    }
                    var args = _.object(['command', 'page_name', 'bg_width', 'bg_height', 'roll_table', 'roll_name'], msg.content.split(' '));
                    args.bg_width = parseInt(args.bg_width);
                    args.bg_height = parseInt(args.bg_height);
                    fill_background(args)
                }
                catch (err) {
                    log('SowaRandomEncounters: Something went wrong bois')
                }
            }
            else if (msg.content.startsWith('!sowa-spawn-trees')) {
                try {
                    if (msg.content.split(' ').length !== 8) {
                        log('SowaRandomEncounters: !sowa-spawn-tress pagen_name sparsity width height r_size roll_table roll_name');
                        return
                    }
                    var args = _.object(['command', 'page_name', 'sparsity', 'width', 'height', 'r_size', 'roll_table', 'roll_name'], msg.content.split(' '));
                    args.sparsity = parseInt(args.sparsity);
                    args.width = parseInt(args.width);
                    args.height = parseInt(args.height);
                    args.r_size = parseInt(args.r_size);
                    spawm_trees(args);
                }
                catch (err) {
                    log('SowaRandomEncounters: Something went wrong bois')
                }
            }
        }

        function register_event_handlers() {
            on('chat:message', handle_input)
        }

        return {
            register_event_handlers: register_event_handlers
        };
    }
    ();

on('ready', function () {
    SowaRandomEncounters.register_event_handlers();
    log('we done here bois')
});