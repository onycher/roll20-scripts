
var SowaSpellSlots = SowaSpellSlots || (function() {
    "use strict";

    function makeNormalMessage(text) {
	  return '<div style="border: 1px solid black; background-color: white; padding: 3px 3px;">' +
	    '<div style="font-weight: bold; border-bottom: 1px solid black;font-size: 130%;">' +
	    `Expend spell slots </div>${text}</div>`;
	}

	function makeErrorMessage(text) {
	  return '<div style="border: 1px solid black; background-color: white; padding: 3px 3px;">' +
	    '<div style="font-weight: bold; border-bottom: 1px solid black;font-size: 130%;color:red;">' +
	    `Expend spell slots </div>${text}</div>`;
	}

    var error = function(player_id, error) {
        let player = getObj("player", player_id);
        let player_name = player.get("displayname");
        sendChat("SYSTEM", "/w \"" + player_name + "\" " + error);
    };

    var expend_spell_slot = function(player_id, character_id, slot_level) {
        var attr_name = "spell_slots_l" + slot_level;
        var spell_slot_attr = findObjs({type: "attribute", characterid: character_id, name: attr_name});
        if(spell_slot_attr.length !== 1) {
            error(player_id, makeErrorMessage("Wrong spell slot"));
            return;
        }
        spell_slot_attr = spell_slot_attr[0];
        if(spell_slot_attr.get("current") < 1) {
            error(player_id, makeNormalMessage("No more spell slots for " + slot_level + " level"));
            return;
        }
        spell_slot_attr.set("current", spell_slot_attr.get("current") - 1);
    };

    var handle_expend_spell_slot = function(msg) {
        var args = msg.content.trim().split(" ");
        var slot_level = args[1];
        var player_id = msg.playerid;
        var selected = msg.selected;
        var player = getObj("player", player_id);
        var character_id;
        var character;
        if(selected === undefined) {
            error(player_id, makeErrorMessage("Select token"));
            return;
        }
        if(selected.length !== 1) {
            error(player_id, makeErrorMessage("Select only one token"));
            return;
        }
        selected = selected[0];
        selected = getObj(selected._type, selected._id);
        if(selected.get("subtype") !== "token") {
            error(player_id, makeErrorMessage("Select token"));
        }
        character_id = selected.get("represents");
        if(character_id === "") {
            error(player_id, makeErrorMessage("No character assigned to token"));
            return;
        }
        character = getObj("character", character_id);
        expend_spell_slot(player_id, character_id, slot_level);
    };

    var handle_input = function(msg_orig) {
        var msg = _.clone(msg_orig);
        if (msg.type !== "api") {
            return;
        }
        if(msg.content.match(/^!sowa-expend-spell-slot \d$/)) {
            handle_expend_spell_slot(msg);
        }
    };

    var register_event_handlers = function() {
        on("chat:message", handle_input);
    };

    return {
        RegisterEventHandlers: register_event_handlers
    };

})();

on("ready", function() {
    "use strict";
    SowaSpellSlots.RegisterEventHandlers();
    log("We ready bois!");
});